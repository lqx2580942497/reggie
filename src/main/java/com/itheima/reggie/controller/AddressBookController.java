package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.AddressBook;
import com.itheima.reggie.service.AddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("addressBook")
@Slf4j
public class AddressBookController {
    @Autowired
    private AddressBookService addressBookService;

    /**
     * 新增收货地址
     * @param addressBook
     * @return
     */
    @PostMapping
    public R<String> saveAddress(@RequestBody AddressBook addressBook, HttpServletRequest request) {
        //从session中获取当前用户id
        Long userId = (Long) request.getSession().getAttribute("user");
        //绑定地址与用户id
        addressBook.setUserId(userId);
        //保存
        addressBookService.save(addressBook);
        return R.success("新增地址成功");
    }

    /**
     * 获取用户地址薄
     * @param request 根据session里的用户id查询
     * @return
     */
    @GetMapping("/list")
    public R<List<AddressBook>> getAddressBookList(HttpServletRequest request) {
        //从session中获取当前用户id
        Long userId = (Long) request.getSession().getAttribute("user");
        //构造查询条件
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId, userId);
        //查询
        List<AddressBook> list = addressBookService.list(queryWrapper);
        return R.success(list);
    }

    /**
     * 更新默认地址（默认地址唯一：将用户的所有地址的默认值设为0，再将指定地址的默认值设为1）
     * @param addressBook
     * @param request
     * @return
     */
    @PutMapping("/default")
    public R<String> isDefault(@RequestBody AddressBook addressBook, HttpServletRequest request) {
        //1.将用户的所有地址的默认值设为0
        //从session中获取当前用户id
        Long userId = (Long) request.getSession().getAttribute("user");
        //构造条件
        UpdateWrapper<AddressBook> queryWrapper1 = new UpdateWrapper<>();
        queryWrapper1.eq("user_id", userId);
        queryWrapper1.set("is_default", 0);
        //执行
        addressBookService.update(queryWrapper1);
        //2.将指定地址的默认值设为1
        //构造条件
        UpdateWrapper<AddressBook> queryWrapper2 = new UpdateWrapper<>();
        queryWrapper2.eq("user_id", userId);
        //指定地址
        queryWrapper2.eq("id",addressBook.getId());
        queryWrapper2.set("is_default", 1);
        //执行
        addressBookService.update(queryWrapper2);
        return R.success("设置成功");
    }

    /**
     * 查询地址（在编辑页面进行数据回显）
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<AddressBook> editAddress(@PathVariable Long id){
        return R.success(addressBookService.getById(id));
    }

    /**
     * 更新地址信息
     * @param addressBook
     * @return
     */
    @PutMapping
    public R<String> updateAddress(@RequestBody AddressBook addressBook){
        addressBookService.updateById(addressBook);
        return R.success("编辑成功");
    }

    /**
     * 删除地址信息
     * @param ids
     * @return
     */
    @DeleteMapping
    public R<String> deleteAddress(@RequestParam Long ids){
        log.info("删除id:{}",ids);
        addressBookService.removeById(ids);
        return R.success("删除成功");
    }

    /**
     * 查询默认地址
     * @return
     */
    @GetMapping("/default")
    public R<AddressBook> getAddressDefault(){
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId, BaseContext.getCurrentId());
        queryWrapper.eq(AddressBook::getIsDefault,1);
        AddressBook addressBook = addressBookService.getOne(queryWrapper);
        if (addressBook==null){
            return R.error("没有默认地址");
        }
        return R.success(addressBook);
    }
}
