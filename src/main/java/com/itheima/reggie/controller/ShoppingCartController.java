package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.ShoppingCart;
import com.itheima.reggie.service.ShoppingCartServive;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/shoppingCart")
@Slf4j
public class ShoppingCartController {
    @Autowired
    private ShoppingCartServive shoppingCartServive;

    /**
     * 展开购物车
     *
     * @param request
     * @return
     */
    @GetMapping("/list")
    public R<List<ShoppingCart>> getShoppingCartList(HttpServletRequest request) {
        Long userId = (Long) request.getSession().getAttribute("user");
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, userId);
        queryWrapper.orderByDesc(ShoppingCart::getCreateTime);
        List<ShoppingCart> list = shoppingCartServive.list(queryWrapper);
        return R.success(list);
    }

    /**
     * 添加商品进购物车，如果购物车内已有，则份数+1
     *
     * @param shoppingCart
     * @param request
     * @return
     */
    @PostMapping("/add")
    public R<ShoppingCart> addShoppingCart(@RequestBody ShoppingCart shoppingCart, HttpServletRequest request) {
        Long userId = (Long) request.getSession().getAttribute("user");
        shoppingCart.setUserId(userId);
        //判断是菜品还是套餐
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        if (shoppingCart.getDishId() != null) {
            queryWrapper.eq(ShoppingCart::getDishId, shoppingCart.getDishId());
        } else {
            queryWrapper.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
        }
        queryWrapper.eq(ShoppingCart::getUserId, userId);
        //查询购物车内是否已有
        ShoppingCart one = shoppingCartServive.getOne(queryWrapper);
        if (one != null) {
            //已有，份数+1
            int number = one.getNumber();
            one.setNumber(number + 1);
            shoppingCartServive.updateById(one);
        } else {
            //没有，添加进购物车
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartServive.save(shoppingCart);
            one = shoppingCart;
        }

        return R.success(one);
    }

    /**
     * 减少份数（如果份数为0则直接删除数据）
     *
     * @param shoppingCart
     * @return
     */
    @PostMapping("/sub")
    public R<String> sub(@RequestBody ShoppingCart shoppingCart) {
        Long dishId = shoppingCart.getDishId();
        Long setmealId = shoppingCart.getSetmealId();
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        if (dishId != null) {
            queryWrapper.eq(ShoppingCart::getDishId, dishId);
        } else {
            queryWrapper.eq(ShoppingCart::getSetmealId, setmealId);
        }
        //查询到的菜品/套餐
        ShoppingCart one = shoppingCartServive.getOne(queryWrapper);
        //获取份数
        int number = one.getNumber();
        //判断
        if (number == 1) {
            //直接移除数据
            shoppingCartServive.remove(queryWrapper);
        } else {
            //份数减少1，更新购物车表
            number -= 1;
            one.setNumber(number);
            shoppingCartServive.updateById(one);
        }
        return R.success("减少成功");
    }

    /**
     * 清空购物车
     *
     * @param request
     * @return
     */
    @DeleteMapping("/clean")
    public R<String> deleteAll(HttpServletRequest request) {
        Long userId = (Long) request.getSession().getAttribute("user");
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, userId);
        shoppingCartServive.remove(queryWrapper);
        return R.success("清空成功");
    }
}
