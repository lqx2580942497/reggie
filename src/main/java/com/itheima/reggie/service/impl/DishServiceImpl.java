package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.DishFlavor;
import com.itheima.reggie.mapper.DishMapper;
import com.itheima.reggie.service.DishFlavorService;
import com.itheima.reggie.service.DishService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {
    @Autowired
    private DishFlavorService dishFlavorService;

    /**
     * 新增菜品同时插入菜品对应的口味，需要操作两张表，dish、dish_flavor
     * @param dishDto
     */
    @Override
    @Transactional//涉及多表操作，需要加入事务控制
    public void saveWithFlavor(DishDto dishDto) {
        //1.保存基本信息到dish表
        this.save(dishDto);
        //2.保存口味到口味表
        //获取菜品id
        Long dishId = dishDto.getId();
        //获取菜品口味
        List<DishFlavor> flavors = dishDto.getFlavors();
        //用stream流的方式建立id和口味的联系
        flavors = flavors.stream().map((item) -> {
            item.setDishId(dishId);
            return item;
        }).collect(Collectors.toList());
        //批量保存
        dishFlavorService.saveBatch(flavors);
    }

    /**
     * 根据id查询菜品信息和对应的口味信息
     * @param id
     * @return
     */
    @Override
    @Transactional//涉及多表操作，需要加入事务控制
    public DishDto getByIdWithFlavor(Long id) {
        //查询菜品基本信息，从dish表查询
        Dish dish = this.getById(id);
        DishDto dishDto = new DishDto();
        //将dish的值拷贝到dishDto中
        BeanUtils.copyProperties(dish,dishDto);

        //查询当前菜品对应的口味信息，从dish_flavor表查询
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId,id);
        List<DishFlavor> flavors = dishFlavorService.list(queryWrapper);
        //将口味信息赋值给dishDto
        dishDto.setFlavors(flavors);
        return dishDto;
    }

    /**
     * 更新菜品信息,同时更新口味信息
     * @param dishDto
     */
    @Override
    @Transactional//加入事务，保证数据一致性
    public void updateWithFlavor(DishDto dishDto) {
        //1.更新菜品表信息
        this.updateById(dishDto);
        //2.更新口味表信息
        //2.1删除当前菜品对应口味数据-----对dish_flavor表进行delete操作
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId,dishDto.getId());
        dishFlavorService.remove(queryWrapper);
        //2.2保存提交过来的口味数据-----对dish_flavor表进行insert操作
        List<DishFlavor> flavors = dishDto.getFlavors();
        //用stream流的方式建立菜品id和口味的联系
        flavors = flavors.stream().map((item) -> {
            item.setDishId(dishDto.getId());
            return item;
        }).collect(Collectors.toList());
        //批量保存
        dishFlavorService.saveBatch(flavors);

    }
}
