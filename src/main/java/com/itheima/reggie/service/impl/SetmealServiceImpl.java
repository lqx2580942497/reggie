package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.DishFlavor;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.entity.SetmealDish;
import com.itheima.reggie.mapper.SetmealMapper;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {
    @Autowired
    private SetmealDishService setmealDishService;
    /**
     * 新增套餐，同时需要保存套餐和菜品的关联关系
     * @param setmealDto
     */
    @Override
    @Transactional//涉及多表操作，需要加入事务控制
    public void saveWithDish(SetmealDto setmealDto) {
        //1.保存套餐的基本信息表------在setmeal执行insert
        this.save(setmealDto);
        //2.保存套餐和菜品关联信息------在setmeal_dish执行insert
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        //遍历setmealDishes，给每个SetmealDish实体里的setmealId赋值
        setmealDishes.stream().map((item)->{
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());
        //批量保存
        setmealDishService.saveBatch(setmealDishes);
    }

    /**
     * 删除套餐，同时需要删除套餐和菜品的关联数据
     * @param ids
     */
    @Override
    @Transactional//涉及多表操作，需要加入事务控制
    public void removeWithDish(List<Long> ids) {
        //1.查询套餐状态，确定是否可以删除（只有停售才可以删除）
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(Setmeal::getId,ids);
        queryWrapper.eq(Setmeal::getStatus,1);
        int count = this.count(queryWrapper);
        //如果不能删除，抛出一个异常信息
        if (count>0){
            throw new CustomException("套餐正在售卖中，不能删除");
        }
        //2.如果可以删除，先删除套餐表中的数据------setmeal
        this.removeByIds(ids);
        //3.删除关系表中的数据------setmeal------setmeal_dish
        LambdaQueryWrapper<SetmealDish> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(SetmealDish::getSetmealId,ids);
        setmealDishService.remove(lambdaQueryWrapper);
    }

    /**
     * 根据id查询套餐和对应的菜品信息
     *
     * @param id
     * @return
     */
    @Override
    @Transactional//涉及多表操作，需要加入事务控制
    public SetmealDto getByIdWithDish(Long id) {
        SetmealDto setmealDto = new SetmealDto();
        //1.查询套餐信息------setmeal表
        Setmeal setmeal = this.getById(id);
        //拷贝
        BeanUtils.copyProperties(setmeal,setmealDto);
        //2.查询对应菜品信息------setmeal_dish表
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId,id);
        List<SetmealDish> setmealDishes = setmealDishService.list(queryWrapper);
        //赋值
        setmealDto.setSetmealDishes(setmealDishes);
        return setmealDto;
    }

    /**
     * 修改套餐，同时修改
     * @param setmealDto
     */
    @Override
    @Transactional//涉及多表操作，需要加入事务控制
    public void updateWithDish(SetmealDto setmealDto) {
        //1.修改套餐信息表
        this.updateById(setmealDto);
        //2.修改套餐菜品信息表（先删除，再重新插入）
        //2.1删除旧菜品
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId,setmealDto.getId());
        setmealDishService.remove(queryWrapper);
        //2.2插入新菜品
        List<SetmealDish> list = setmealDto.getSetmealDishes();
        //用stream流的方式建立套餐id和菜品的联系
        list = list.stream().map((item) -> {
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());
        //批量保存
        setmealDishService.saveBatch(list);
    }
}
